<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use Ucc\Models\Question;

class QuestionTest extends TestCase
{

    /**
     * @test
     */
    public function when_an_answer_is_correct_true_is_returned()
    {
        $question = new Question;

        $question->setCorrectAnswer('test');

        $this->assertTrue($question->matches('test'));
    }
}
