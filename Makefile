.PHONY: test

test:
	./vendor/bin/phpstan analyse src tests --level 1
	./vendor/bin/phpcbf -p --standard=PSR2 src tests || true
	./vendor/bin/phpunit --configuration ./phpunit.xml tests