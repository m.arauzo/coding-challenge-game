<?php

namespace Ucc\Repositories;

use JsonMapper;
use KHerGe\JSON\JSON;
use Ucc\Models\Question;
use Ucc\Models\QuestionNotFoundException;
use Ucc\Models\QuestionRepository;

class JSONQuestionRepository implements QuestionRepository
{
    private const QUESTIONS_PATH = __DIR__ . '/../../questions.json';

    private JSON $json;
    private JsonMapper $jsonMapper;

    public function __construct(JSON $json, JsonMapper $jsonMapper)
    {
        $this->json = $json;
        $this->jsonMapper = $jsonMapper;
    }

    public function randomQuestions(int $count = 5): array
    {
        $questions = $this->json->decodeFile(self::QUESTIONS_PATH);

        $found = [];

        for ($i=0; $i < $count; $i++) {
            $randomNumber = rand(0, 8);
            $questionObject = $this->jsonMapper->map($questions[$randomNumber], new Question);
            $found[] = $questionObject;
        }

        return $found;
    }

    public function byIdOrFail(int $id): Question
    {
        $questions = $this->json->decodeFile(self::QUESTIONS_PATH);

        // since questions are ordered by id, the index is always the id minus one
        if (array_key_exists($id - 1, $questions)) {
            throw new QuestionNotFoundException(sprintf("Question with id %s was not found", $id));
        }

        return $this->jsonMapper->map($questions[$id - 1], new Question);
    }
}
