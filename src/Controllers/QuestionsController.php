<?php


namespace Ucc\Controllers;

use Ucc\Http\JsonResponseTrait;
use Ucc\Models\QuestionNotFoundException;
use Ucc\Services\QuestionService;
use Ucc\Session;

class QuestionsController extends Controller
{
    use JsonResponseTrait;

    private QuestionService $questionService;

    public function __construct(QuestionService $questionService)
    {
                    parent::__construct();
        $this->questionService = $questionService;
    }

    public function beginGame(): bool
    {
        $name = $this->requestBody->name ?? null;
        if (empty($name)) {
            return $this->json('You must provide a name', 400);
        }

        Session::set('name', $name);
        Session::set('questionCount', 1);

        $question = $this->questionService->getRandomQuestions(1);

        return    $this->json(['question' => json_encode($question)], 201);
    }

    public function answerQuestion(int $id): bool
    {
        // @todo could be moved into a middleware where session is checked
        if (Session::get('name') === null) {
            return $this->json('You must first begin a game', 400);
        }

        if ((int)Session::get('questionCount') > 4) {
            $name = Session::get('name');
            $points = Session::get('points');
            Session::destroy();
            return $this->json(['message' => "Thank you for playing {$name}. Your total score was: {$points} points!"]);
        }

        $answer = $this->requestBody->answer ?? null;
        if (empty($answer)) {
            return $this->json('You must provide an answer', 400);
        }

        Session::set('questionCount', Session::get('questionCount') + 1);

        $message = 'Good job!';

        try {
            $points = $this->questionService->getPointsForAnswer($id, $answer);
            
            if ($points === 0) {
                $message = 'Sorry you answered wrong';
            }

            Session::set('points', Session::get('points') + $points);
        } catch (QuestionNotFoundException $e) {
            return $this->json(["error" => "Question was not found"], 404);
        }

        $question = $this->questionService->getRandomQuestions(7);

        return $this->json(['message' => $message, 'question' => $question]);
    }
}
