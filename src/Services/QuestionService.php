<?php


namespace Ucc\Services;

use JsonMapper;
use KHerGe\JSON\JSON;
use Ucc\Models\Question;
use Ucc\Models\QuestionNotFoundException;

class QuestionService
{
    private const QUESTIONS_PATH = __DIR__ . '/../../questions.json';

    private JSON $json;
    private JsonMapper $jsonMapper;

    // @todo could be replaced by a repository abstracting the way we deal with data (no specific dependency)
    // and making questionService open to new data types
    // and easily test it
    public function __construct(JSON $json, JsonMapper $jsonMapper)
    {
        $this->json = $json;
        $this->jsonMapper = $jsonMapper;
    }

    public function getRandomQuestions(int $count = 5): array
    {
        $questions = $this->json->decodeFile(self::QUESTIONS_PATH);

        $found = [];

        for ($i=0; $i < $count; $i++) {
            $randomNumber = rand(0, 7);
            $questionObject = $this->jsonMapper->map($questions[$randomNumber], new Question);
            $found[] = $questionObject;
        }

        return $found;
    }

    public function getPointsForAnswer(int $id, string $answer): int
    {
        $questions = $this->json->decodeFile(self::QUESTIONS_PATH);

        if (!isset($questions[$id - 1])) {
            throw new QuestionNotFoundException(sprintf("Question with id %s was not found", $id));
        }

        // since questions are ordered by id, the index is always the id minus one
        $question = $this->jsonMapper->map($questions[$id - 1], new Question);

        if ($question->matches($answer)) {
            return $question->getPoints();
        }

        return 0;
    }
}
