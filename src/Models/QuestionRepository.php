<?php

namespace Ucc\Models;

interface QuestionRepository
{
    public function randomQuestions(int $count = 5): array;

    public function byIdOrFail(int $id): Question;
}
